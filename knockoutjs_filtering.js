var ajaxUrl = "/montra/loadmore";
var modalViewModel = {
	title: ko.observable(),
	paragraphs: ko.observableArray(),
	buttons: ko.observableArray(),
	productCode: ko.observable(),
	MobileModalBuy: function(item) {
		var data = {
			product_code: PageViewModel.modalViewModel.productCode(),
			product_size: item.stockID,
			product_qtd: 1,
			product_detail: 'n',
			product_id: item.product_id,
			product_name: item.product_name,
			product_price: item.price
		};
		url = "/carrinho-compras/add-to-cart/";
		addToCart(data, url, false);;
	}
};
var filterViewModel = {
	categorySlugArray: ko.observableArray([]),
	order: ko.observable('relevancia'),
	page: 0,
	minPrice: ko.observable(''),
	maxPrice: ko.observable(''),
	pricesMin: ko.observableArray([]),
	pricesMax: ko.observableArray([]),
	sizes: ko.observableArray([]),
	selectedSizes: ko.observableArray(),
	brands: ko.observableArray([]),
	emptyBrands: ko.observable(true),
	selectedBrandIDs: ko.observableArray(),
	subSubCategories: ko.observableArray(),
	campaigns: ko.observableArray([]),
	selectedCampaigns: ko.observableArray(['03c1cd681f55f1d01ea4a63f1095eac6e430ba91']),
	totalProducts: '',
	categoryStructure: ko.observableArray([]),
	orderByCat: ["", "", "", ""],
	allPagesFromStart: false,
	getFilters: function() {
		return {
			categorySlugArray: this.categorySlugArray(),
			order: this.order(),
			selectedCampaigns: this.selectedCampaigns(),
			page: this.page,
			selectedSizes: this.selectedSizes(),
			selectedBrandIDs: this.selectedBrandIDs(),
			minPrice: this.minPrice(),
			maxPrice: this.maxPrice(),
			orderByCat: this.orderByCat,
			allPagesFromStart: this.allPagesFromStart
		}
	}
};
if (history.state && history.state.filterViewModel) {
	oFVM = history.state.filterViewModel;
	filterViewModel.categorySlugArray = ko.observableArray(oFVM.categorySlugArray);
	filterViewModel.order = ko.observable(oFVM.order);
	filterViewModel.selectedCampaigns = ko.observableArray(oFVM.selectedCampaigns);
	filterViewModel.page = oFVM.page;
	filterViewModel.selectedSizes = ko.observableArray(oFVM.selectedSizes);
	filterViewModel.selectedBrandIDs = ko.observableArray(oFVM.selectedBrandIDs);
	filterViewModel.minPrice = ko.observable(oFVM.minPrice);
	filterViewModel.maxPrice = ko.observable(oFVM.maxPrice);
	filterViewModel.orderByCat = oFVM.orderByCat;
	filterViewModel.allPagesFromStart = true;
	if (history.state.lastProduct) {
		document.location = "#product_" + history.state.lastProduct;
	}
	history.replaceState({}, "", "");
}

function ProductViewModel() {
	var self = this;
	self.ProductList = ko.observableArray([]);
	self.isLoading = ko.observable(true);
	self.ajaxPending = ko.observable(false);
	self.GetProducts = function(cleanList) {
		if (cleanList) {
			$('body,html').animate({
				scrollTop: 0
			}, "slow");
		}
		$.ajax({
			type: "POST",
			data: filterViewModel.getFilters(),
			url: ajaxUrl,
			success: function(res) {
				filterViewModel.allPagesFromStart = false;
				filterViewModel.selectedSizes(res.selectedSizes);
				filterViewModel.selectedBrandIDs(res.selectedBrandIDs);
				filterViewModel.selectedCampaigns(res.selectedCampaigns);
				filterViewModel.categorySlugArray(res.categorySlugArray);
				if (typeof(res.categorySlugArray[3]) != 'undefined') {
					filterViewModel.subSubCategories(res.categorySlugArray[3].split('|'));
				}
				if (cleanList) {
					self.ProductList.removeAll();
				}
				$.each(res.products, function(key, product) {
					self.ProductList.push(new ProductDetail(product));
				});
				gtmTrackProductList(res.products);
				gtmTrackCategoryView(res.selectedCampaigns, res.categorySlugArray);
				$('#fill_content').css('display', 'block');
				filterViewModel.sizes(res.sizes);
				$.each(res.sizes, function(key, size) {
					if (filterViewModel.selectedSizes().indexOf(size.tamanho) >= 0) {
						res.sizes.splice(key, 1);
						res.sizes.unshift(size);
					}
				});
				filterViewModel.sizes(res.sizes);
				filterViewModel.brands(res.brands);
				var emptyBrands = res.brands.length > 1 ? false : true;
				filterViewModel.emptyBrands(emptyBrands);
				var pricesMin = res.prices.slice();
				pricesMin.splice((res.prices.length - 2), 2);
				var pricesMax = res.prices.slice();
				pricesMax.splice(0, 2);
				filterViewModel.pricesMin(pricesMin);
				filterViewModel.pricesMax(pricesMax);
				filterViewModel.totalProducts = res.totalProducts;
				$.each(res.campaigns, function(key, campaign) {
					if (filterViewModel.selectedCampaigns().indexOf(campaign.internal_code) >= 0) {
						res.campaigns.splice(key, 1);
						res.campaigns.unshift(campaign);
					}
				});
				filterViewModel.campaigns(res.campaigns);
				filterViewModel.categoryStructure.removeAll();
				$.each(res.categoryStructure, function(key, item) {
					filterViewModel.categoryStructure.push(new CategoryStructure(item));
				});
				self.isLoading(false);
				self.ajaxPending(false);
			},
			complete: function() {
				$('.flexslider_product:not(.flexslider-active)').flexslider({
					slideshow: false,
					controlNav: false
				});
				$('.flexslider_product:not(.flexslider-active)').addClass('flexslider-active');
			}
		});
	};
	self.GetProducts(false);
}

function CategoryStructure(categoryStructure) {
	var self = this;
	self.areaNegocioSlug = categoryStructure.area_negocio_slug;
	self.count = categoryStructure.count;
	self.id = categoryStructure.id;
	self.idPai = categoryStructure.id_pai;
	self.nome = categoryStructure.nome;
	self.slug = categoryStructure.slug;
	self.children = [];
	self.grayOut = categoryStructure.gray_out;
	if (typeof(categoryStructure.children) != 'undefined') {
		$.each(categoryStructure.children, function(key, item) {
			self.children.push(new CategoryStructure(item));
		});
	}
}

function ProductDetail(product) {
	var self = this;
	self.id = product.id;
	self.nome = product.nome;
	self.internal_code = product.internal_code;
	self.stock_available = product.stock_available;
	self.discount = parseFloat(product.discount);
	self.preco_cf = parseFloat(product.preco_cf);
	self.preco_pvp = product.preco_pvp;
	self.url = product.url;
	self.imagens = product.imagens;
	self.stocks = product.stocks;
	self.cname = product.cname;
	self.processProductClick = function(product) {
		history.replaceState({
			filterViewModel: filterViewModel.getFilters(),
			lastProduct: product.id
		}, "", "");
		dataLayer.push({
			'event': 'productClick',
			"ecommerce": {
				'currencyCode': 'EUR',
				'click': {
					actionField: {
						'list': product.cname
					},
					products: [{
						'name': product.nome,
						'id': product.id,
						'price': product.preco_cf
					}]
				}
			}
		});
		return true;
	};
	self.selectedSizeID = ko.observable('');
	if (product.stocks.length == 1) {
		self.selectedSizeID(product.stocks[0].id);
	}
	self.chooseSize = function(size) {
		if (size.stock_available > 0) {
			self.selectedSizeID(size.id);
		}
	};
	self.buyProduct = function() {
		var price = 0;
		selectedSize = self.selectedSizeID();
		price = self.stocks.some(function(stock) {
			if (stock.id == selectedSize) {}
		});
		var data = {
			'product_size': self.selectedSizeID(),
			'product_qtd': '1',
			'product_code': self.internal_code,
			'product_detail': 'n',
			'product_name': product.nome,
			'product_price': price,
			'product_id': self.id
		};
		url = "/carrinho-compras/add-to-cart/";
		addToCart(data, url, false);
	};
	self.expressBuy = function(product) {
		if (product.selectedSizeID() == '') {
			if ($(window).width() < 767) {
				modalViewModel.title('Informação');
				modalViewModel.paragraphs([{
					content: 'Por favor, escolha um tamanho.'
				}, ]);
				modalViewModel.buttons.removeAll();
				$.each(product.stocks, function(key, item) {
					modalViewModel.buttons.push({
						content: item.tamanho,
						primary: false,
						stockID: item.id,
						price: item.preco_cf,
						product_id: product.id,
						product_name: product.nome,
						selectSizeAndBuy: true,
						stockAvailable: (item.stock_available > 0 ? true : false)
					});
				});
				modalViewModel.buttons.push({
					content: 'Fechar',
					primary: false,
					stockID: '',
					selectSizeAndBuy: false,
					stockAvailable: true
				});
				modalViewModel.productCode(product.internal_code);
				$('#product-list-modal').modal();
				return;
			}
			modalViewModel.title('Informação');
			modalViewModel.paragraphs([{
				content: 'Por favor, escolha um tamanho.'
			}, ]);
			modalViewModel.buttons([{
				content: 'Fechar',
				primary: true,
				stockID: '',
				selectSizeAndBuy: false,
				stockAvailable: true
			}]);
			$('#product-list-modal').modal();
			return;
		}
		self.buyProduct();
	}
}

function PageViewModel() {
	var self = this;
	self.firstLoad = ko.observable(true);
	self.filterViewModel = filterViewModel;
	self.ProductViewModel = new ProductViewModel();
	self.modalViewModel = modalViewModel;
	self.ClearFiltersAndFetch = function() {
		self.firstLoad(true);
		self.filterViewModel.categorySlugArray.removeAll();
		self.filterViewModel.minPrice('');
		self.filterViewModel.maxPrice('');
		self.filterViewModel.selectedSizes.removeAll();
		self.filterViewModel.subSubCategories.removeAll();
		self.filterViewModel.selectedCampaigns.removeAll();
		self.filterViewModel.selectedBrandIDs.removeAll();
		self.filterViewModel.orderByCat = ["", "", "", ""];
		self.filterViewModel.selectedCampaigns.push('03c1cd681f55f1d01ea4a63f1095eac6e430ba91');
		self.ProductViewModel.isLoading(true);
		self.filterViewModel.page = 0;
		self.ProductViewModel.ProductList.removeAll();
		self.ProductViewModel.GetProducts(true);
	};
	self.CleanListAndFetch = function() {
		self.firstLoad(false);
		self.filterViewModel.orderByCat = [];
		self.ProductViewModel.isLoading(true);
		self.filterViewModel.page = 0;
		self.ProductViewModel.GetProducts(true);
	};
	self.ChangeCat = function(data, e) {
		if (self.ProductViewModel.isLoading() || self.ProductViewModel.ajaxPending()) {
			return false;
		}
		var elm = $(e.target);
		if (elm.hasClass('disabled')) {
			return false;
		}
		var categoryString = elm.attr('data-category');
		var subSubCategoriesString = '';
		var newCategorySlugArray = categoryString.split(',');
		var arraysEqual = true;
		for (var i = 0; i < 3; i++) {
			if (newCategorySlugArray[i] != self.filterViewModel.categorySlugArray()[i]) {
				arraysEqual = false;
			}
		}
		if (elm.attr('type') == 'checkbox') {
			if (!arraysEqual) {
				self.filterViewModel.subSubCategories.removeAll();
				self.filterViewModel.subSubCategories.push(elm.val());
			}
			subSubCategoriesString = self.filterViewModel.subSubCategories().join('|');
		} else if (self.filterViewModel.subSubCategories().length > 0) {
			self.filterViewModel.subSubCategories.removeAll();
		}
		if (arraysEqual && subSubCategoriesString == '') {
			for (var i = 0; i < 4; i++) {
				if (newCategorySlugArray[i] == '' || i == 3) {
					newCategorySlugArray[i - 1] = '';
				}
			}
		}
		self.filterViewModel.categorySlugArray(newCategorySlugArray);
		self.filterViewModel.categorySlugArray()[3] = subSubCategoriesString;
		self.CleanListAndFetch();
		return false;
	};
	$(window).scroll(function() {
		if (self.ProductViewModel.isLoading()) {
			return false;
		}
		if (self.filterViewModel.page * 30 > self.filterViewModel.totalProducts) {
			return false;
		}
		if (self.ProductViewModel.ajaxPending()) {
			return false;
		}
		if ($(window).scrollTop() + $(window).height() >= ($('#fill_content').height() * 0.75)) {
			self.ProductViewModel.ajaxPending(true);
			self.filterViewModel.page++;
			self.ProductViewModel.GetProducts(false);
		}
	});
}
